# README #

YOLOv3 in OpenCV - IR images

### What is this repository for? ###

This repo contains all necessary files to run YOLO v3 in OpenCV. Contains configuration files, weights and code for Master Thesis subject. 

### How do I get set up? ###

To start with this repo all you need is installed Anaconda and added OpenCV (v3.4.2 +) by anaconda prompt. 

If you will have any problems, please follow instructins from link: https://demystifymachinelearning.wordpress.com/2018/08/30/installing-opencv-on-windows-using-anaconda/

### Who do I talk to? ###

If you will have any concernes please write: aearial94@gmail.com